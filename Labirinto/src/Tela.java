import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import java.awt.SystemColor;

import javax.swing.BorderFactory;
import javax.swing.JMenu;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTable;

public class Tela extends JFrame {

	private JPanel contentPane;
	private JPanel panel;
	
	private Matriz matriz; 
	private JTextField tfNumeroLinhas;
	private JTextField tfNumeroColunas;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				//Muda disgn para o disgn padr�o do SO.
				try {   
				    UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());  
				} catch (UnsupportedLookAndFeelException ex1) {  
				    ex1.printStackTrace();  
				} catch (IllegalAccessException ex2) {  
				    ex2.printStackTrace();  
				} catch (InstantiationException ex3) {  
				    ex3.printStackTrace();  
				} catch (ClassNotFoundException ex4) {  
					ex4.printStackTrace();  
				}			

				Tela frame = new Tela();
				frame.setVisible(true);
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Tela() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 742, 480);
		contentPane = new JPanel();
		contentPane.setBackground(SystemColor.text);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(null);
		setContentPane(contentPane);
		
		renderizarMenu();
		
		renderizarPainel();
		
		renderizarOpcoesMatriz();
		
	}
	
	public void renderizarMenu(){
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBounds(0, 0, 726, 21);
		contentPane.add(menuBar);
		
		JMenu mnConfiguracoes = new JMenu("Configura\u00E7\u00F5es");
		menuBar.add(mnConfiguracoes);
	}
	
	public void renderizarPainel(){
		if(panel != null)
			panel = null;
		
		panel = new JPanel();
		panel.setLayout(null);
		panel.setBounds(173, 49, 543, 381);
		panel.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.BLACK));
		contentPane.add(panel);
	}
	
	public void renderizarOpcoesMatriz(){
		JLabel lblNumeroLinhas = new JLabel("N\u00FAmero de Linhas:");
		lblNumeroLinhas.setBounds(10, 91, 89, 14);
		contentPane.add(lblNumeroLinhas);
		
		tfNumeroLinhas = new JTextField();
		tfNumeroLinhas.setColumns(10);
		tfNumeroLinhas.setBounds(119, 85, 44, 20);
		contentPane.add(tfNumeroLinhas);
		
		JLabel lblNumeroColunas = new JLabel("N\u00FAmero de Colunas:");
		lblNumeroColunas.setBounds(10, 119, 99, 14);
		contentPane.add(lblNumeroColunas);
		
		tfNumeroColunas = new JTextField();
		tfNumeroColunas.setColumns(10);
		tfNumeroColunas.setBounds(119, 113, 44, 20);
		contentPane.add(tfNumeroColunas);
		
		JButton btnGerarMatriz = new JButton("Gerar Matriz");
		btnGerarMatriz.setBounds(43, 154, 99, 23);
		btnGerarMatriz.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(matriz != null)
					renderizarPainel();
				matriz = new Matriz(Integer.parseInt(tfNumeroLinhas.getText()), Integer.parseInt(tfNumeroColunas.getText()));
				matriz.gerarMatriz(panel);			
			}
		});
		contentPane.add(btnGerarMatriz);
	}
}

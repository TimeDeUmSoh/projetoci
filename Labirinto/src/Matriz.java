import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;

public class Matriz {
	
	private int numeroLinhas;
	private int numeroColunas;
	private List<Celula> celulas;	
	
	private int pontoPartida;
	private int pontoChegada;
		
	public Matriz(int numeroLinhas, int numeroColunas) {
		super();
		this.numeroLinhas = numeroLinhas;
		this.numeroColunas = numeroColunas;
		
		this.celulas = new ArrayList<Celula>();
	}
		
	public void gerarMatriz(JPanel painelMatriz){
		int larguraPainel = (int) painelMatriz.getBounds().getWidth();
		int alturaPainel = (int) painelMatriz.getBounds().getHeight();
		
		int larguraCelula = larguraPainel/numeroColunas;
		int alturaCelula = alturaPainel/numeroLinhas;
		
		int idCelula = 1;
		
		int coordLinha = 0;
		
		for(int linha = 0; linha < numeroLinhas; linha++){
			int coordColuna = 0;
			for(int coluna = 0; coluna < numeroColunas; coluna++){
				Celula celula = new Celula(idCelula);
				celula.desenharCelula(coordLinha, coordColuna, coordLinha + larguraCelula, coordColuna + alturaCelula, painelMatriz);
				celulas.add(celula);
			
				coordColuna = coordColuna + alturaCelula;
				idCelula++;
			}
			coordLinha = coordLinha + larguraCelula;			
		}
	}

	public int getNumeroLinhas() {
		return numeroLinhas;
	}
	
	public void setNumeroLinhas(int numeroLinhas) {
		this.numeroLinhas = numeroLinhas;
	}
	
	public int getNumeroColunas() {
		return numeroColunas;
	}
	
	public void setNumeroColunas(int numeroColunas) {
		this.numeroColunas = numeroColunas;
	}

}

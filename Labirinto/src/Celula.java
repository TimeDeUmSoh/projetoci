import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;

public class Celula {
	private int idCelula;
	private Integer valorCelula;
	private JLabel celula;
	
	public Celula(int idCelula) {
		super();
		this.idCelula = idCelula;
	}
	
	public void desenharCelula(int coordX, int coordY, int largura, int altura, JPanel painelMatriz){
		celula = new JLabel();
		celula.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				celula.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent arg0) {
				        mostrarPopupMenu(arg0);
					}
				});
			}
		});

		celula.setBounds(coordX, coordY, largura, altura);
		celula.setBorder(BorderFactory.createMatteBorder(1, 1, 0, 0, Color.BLACK));
		painelMatriz.add(celula);
		painelMatriz.repaint();
		
		
	}
	
	private void mostrarPopupMenu(java.awt.event.MouseEvent evt) {
        if (evt.getButton() == MouseEvent.BUTTON3) { // Clique com botao direito do mouse. 
        	JPopupMenu menu = new JPopupMenu();
			
			JMenu mnConfiguracoes = new JMenu("Configura\u00E7\u00F5es");
			menu.add(mnConfiguracoes);
			
			menu.setVisible(true);
        }          
    }
	
	public int getIdCelula() {
		return idCelula;
	}

	public void setIdCelula(int idCelula) {
		this.idCelula = idCelula;
	}
	
	public int getValorCelula() {
		return valorCelula;
	}

	public void setValorCelula(Integer valorCelula) {
		this.valorCelula = valorCelula;
	}
}
